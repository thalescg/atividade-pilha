package br.cesed.si.p3.stack.sequencial;

public class MinhaPilha {

	private static final int TAMANHO_INICIAL = 3;
	
	private Object[] arrayInterno = new Object[TAMANHO_INICIAL];
	
	private int inseridos;
	
	public void push(Object objeto) throws OperacaoInvalidaException {
		
		if(objeto == null) {
			
			throw new OperacaoInvalidaException();
			
		}		
		
		if(inseridos == arrayInterno.length) {
			
			Object[] arrayTemporario = new Object[TAMANHO_INICIAL + 3];
			
			for (int i = 0; i < arrayInterno.length; i++) {
				
				arrayTemporario[i] = arrayInterno[i];
								
			}
			
			arrayInterno = arrayTemporario;
			
		}
		
		arrayInterno[inseridos] = objeto;
		inseridos ++;
		
	}
	
	public Object pop() throws OperacaoInvalidaException {
		
		if(isEmpty()) {
			
			throw new OperacaoInvalidaException();
			
		}
		
		Object temporario = arrayInterno[inseridos-1];
		arrayInterno[inseridos-1] = null;
		inseridos --;
		return temporario;
		
	}

	public Object top() throws OperacaoInvalidaException {
		
		if(isEmpty()) {
			
			throw new OperacaoInvalidaException();
			
		}
		return arrayInterno[inseridos-1];
	}
	
	public int size() {
		return inseridos;
	}

	public boolean isEmpty() {
		if(inseridos == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void imprimir() {
		
		for (int i = 0; i < inseridos; i++) {
			
			System.out.println(arrayInterno[inseridos]);
			
		}
		
		
	}
}
