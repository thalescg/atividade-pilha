package br.cesed.si.p3.stack.sequencial;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MinhaPilhaTest {

	@Test
	void testPush1()  {

		MinhaPilha mp = new MinhaPilha();

		Assert.assertEquals(0, mp.size());
	}

	@Test
	void testPush2() throws OperacaoInvalidaException {

		MinhaPilha mp = new MinhaPilha();
		mp.push("Elemento 1");
		mp.push("Elemento 2");
		mp.push("Elemento 3");

		Assert.assertEquals(3, mp.size());
	}

	@Test
	void testPush3() throws OperacaoInvalidaException {

		MinhaPilha mp = new MinhaPilha();
		mp.push("Elemento 1");
		mp.push("Elemento 2");
		mp.push("Elemento 3");
		mp.push("Elemento 4");

		Assert.assertEquals(4, mp.size());
	}

	@Test
	void testPush4() throws OperacaoInvalidaException {

		MinhaPilha mp = new MinhaPilha();

		Assertions.assertThrows(OperacaoInvalidaException.class, () -> {
			mp.push(null);
		});
		
		Assert.assertEquals(0, mp.size());
	}

	@Test
	void testPop1() throws OperacaoInvalidaException {

		MinhaPilha mp = new MinhaPilha();
		mp.push("Elemento 1");
		mp.pop();

		Assert.assertEquals(0, mp.size());
	}

	@Test
	void testPop2()  {

		MinhaPilha mp = new MinhaPilha();

		Assertions.assertThrows(OperacaoInvalidaException.class, () -> {
			mp.pop();
		});

		Assert.assertEquals(0, mp.size());
	}

	@Test
	void testPop3() throws OperacaoInvalidaException {

		MinhaPilha mp = new MinhaPilha();
		mp.push("Elemento 1");
		mp.push("Elemento 2");
		mp.push("Elemento 3");
		mp.push("Elemento 4");

		Assert.assertEquals("Elemento 4", mp.pop());
		Assert.assertEquals(3, mp.size());
	}

	@Test
	void testTop1() throws OperacaoInvalidaException {

		MinhaPilha mp = new MinhaPilha();
		mp.push("Elemento 1");
		mp.push("Elemento 2");
		mp.push("Elemento 3");
		mp.push("Elemento 4");

		Assert.assertEquals("Elemento 4", mp.top());
		Assert.assertEquals(4, mp.size());
	}

	@Test
	void testTop2() {

		MinhaPilha mp = new MinhaPilha();

		Assertions.assertThrows(OperacaoInvalidaException.class, () -> {
			mp.top();
		});
		
		Assert.assertEquals(0, mp.size());
	}
}
